import Store, { actions } from 'src/store'

export default () => ({
  post(url, data) {
    return new Promise(resolve => {
      let response

      Store.dispatch(actions.IS_LOADING, true)

      if (url === '/conversation/message') {

        switch (data.id) {
          default:
          case 'question_name':
            response = {"id":"question_name","messages":[{"type":"string","value":"Vamos abrir sua conta!^400 Em 4 minutos a conta estará pronta e você vai saber seu perfil de investidor. ^600 Para isso temos que fazer algumas perguntas sobre a sua relação com investimentos.^1000"},{"type":"string","value":"Para começar,^400 como podemos chamar você?^500 (Use seu primeiro nome ou um apelido).^1000"}],"buttons":[],"inputs":[{"type":"string","mask":"name"}],"responses":["Meu nome é {{answers.question_name}}."],"rows":[],"radios":[],"checkbox":[]}
            break;
          case 'question_income':
            response = {"id": "question_invest", "messages": [{"type": "string", "value": "Ok! ^1000 E você já investiu alguma vez? ^1000 Sem considerar a Poupança!"} ], "buttons": [{"value": "A", "label": {"title": "Sim, já investi"} }, {"value": "B", "label": {"title": "Não, nunca investi"} } ], "inputs": [], "responses": [], "rows": [], "radios": [], "checkbox": [] }
            break;
          case 'question_invest':
            response = {"id": "question_investment_time", "messages": [{"type": "string", "value": "Há quanto tempo você já investe?^1000"} ], "buttons": [], "inputs": [], "responses": [], "rows": [], "radios": [{"value": "1", "label": {"title": "Menos de 1 ano"} }, {"value": "1-3", "label": {"title": "Entre 1 e 3 anos"} }, {"value": "3-5", "label": {"title": "Entre 3 e 5 anos"} }, {"value": "5-10", "label": {"title": "Entre 5 e 10 anos"} }, {"value": "10", "label": {"title": "Mais de 10 anos"} } ], "checkbox": [] }
            break;
          case 'question_investment_time':
            response = {"id": "question_interest", "messages": [{"type": "string", "value": "Quando o assunto é administrar seus investimentos, ^500 você acha isso uma tarefa... ^1000"} ], "buttons": [{"value": "A", "label": {"title": "Prazerosa"} }, {"value": "B", "label": {"title": "Entediante"} } ], "inputs": [], "responses": [], "rows": [], "radios": [], "checkbox": [] }
            break;
          case 'question_interest':
            response = {"id": "question_preference", "messages": [{"type": "string", "value": "Agora, ^500 eu preciso saber, ^500 ao montar uma carteira de investimentos, ^500 você prefere… ^1000"} ], "buttons": [{"value": "A", "label": {"title": "Escolher cada produto"} }, {"value": "B", "label": {"title": "Confiar em um especialista"} } ], "inputs": [], "responses": [], "rows": [], "radios": [], "checkbox": [] }
            break;
          case 'question_preference':
            response = {"id": "question_planning", "messages": [{"type": "string", "value": "Certo.^1000 E a respeito de planejamento financeiro,^500 qual comportamento mais se aproxima do seu?^1000"} ], "buttons": [{"value": "A", "label": {"title": "Não planejo muito"} }, {"value": "B", "label": {"title": "Tenho uma boa organização"} } ], "inputs": [], "responses": [], "rows": [], "radios": [], "checkbox": [] }
            break;
          case 'question_planning':
            response = {"id": "question_economy", "messages": [{"type": "string", "value": "Agora vamos imaginar que você fez um investimento há 1 mês e,^500 ao abrir sua conta,^500 vê que seus rendimentos estão negativos.^1000 Qual dessas reações mais se aproxima da sua?^1000"} ], "buttons": [{"value": "A", "label": {"title": "Provavelmente resgataria"} }, {"value": "B", "label": {"title": "Espero e acompanho de perto"} }, {"value": "C", "label": {"title": "Aproveito a baixa e invisto"} } ], "inputs": [], "responses": [], "rows": [], "radios": [], "checkbox": [] }
            break;
          case 'question_economy':
            response = {"id": "question_product", "messages": [{"type": "string", "value": "Ok.^1000 Você já investiu em algum desses produtos?^1000"} ], "buttons": [], "inputs": [], "responses": [], "rows": [], "radios": [], "checkbox": [{"value": "savings", "label": {"title": "Poupança"} }, {"value": "realState", "label": {"title": "Fundo Imobiliário"} }, {"value": "multimarket", "label": {"title": "Fundo Multimercado"} }, {"value": "inflationIndexed", "label": {"title": "Indexados à Inflação"} }, {"value": "cdb", "label": {"title": "CDB e Fundos DI"} }, {"value": "stocks", "label": {"title": "Ações/Fundos de Ações"} }, {"value": "prefixed", "label": {"title": "Prefixados"} } ] }
            break;
          case 'question_product':
            response = {"id": "question_email", "messages": [{"type": "string", "value": "Agora só falta completar seus dados para a conta estar pronta."}, {"type": "string", "value": "Qual o seu e-mail?"} ], "buttons": [], "inputs": [{"type": "string", "mask": "email"} ], "responses": [], "rows": [], "radios": [], "checkbox": [] }
            break;
          case 'question_email':
            response = {"id": "email_suggestion", "messages": [{"type": "string", "value": "Você quis dizer jao@gmail.com?"} ], "buttons": [{"value": "Y", "label": {"title": "Sim, corrigir e-mail"} }, {"value": "N", "label": {"title": "Não, usar o digitado"} }, {"value": "R", "label": {"title": "Digitar novamente"} } ], "inputs": [], "responses": [], "rows": [], "radios": [], "checkbox": [] }
            break;
          case 'email_suggestion':
            response = {"id": "question_password", "messages": [{"type": "string", "value": "Crie uma senha para a sua conta com no mínimo 6 caracteres."} ], "buttons": [], "inputs": [{"type": "string", "mask": "password"} ], "responses": [], "rows": [], "radios": [], "checkbox": [] }
            break;
          case 'question_password':
            response = {"id": "final", "messages": [{"type": "string", "value": "Sua conta está pronta!^800 O seu perfil de investidor é..."} ], "buttons": [], "inputs": [], "responses": [], "rows": [], "radios": [], "checkbox": [] }
            break;
        }

      } else if (url === '/suitability/finish') {
        response = {"user": {"_id": "5f50f398fb72f10017cc2d19", "createdAt": "2020-09-03T13:46:01.760Z", "updatedAt": "2020-09-03T13:46:02.660Z", "dash": "buildFirstPortfolio", "builderType": "default", "status": "E-mail not verified, Register not valid yet", "name": "jao", "email": "jao@gmail.com", "clientType": {"pink": true, "black": false, "partner": false, "betaTester": false, "b2bPartner": false, "whitelabel": false, "banking": false, "trade": false }, "totalBalance": 0, "segmentation": "white", "isDeleted": false, "isLoginLocked": false, "isBlocked": false, "isEmailVerified": false, "isPasswordSet": true, "isSuitabilityStarted": true, "isSuitabilityFinished": true, "isPortfolioStarted": false, "isPortfolioFinished": false, "isPortfolioVisited": false, "isRegisterStarted": false, "isRegisterFinished": false, "isRegisterValidated": false, "isPartnerRegisterVerified": false, "isRegisterVerified": false, "isRegisterRejected": false, "isRegisterModified": false, "isInvestStarted": false, "investStartedCount": 0, "isInvestFinished": false, "isPreregistered": true, "hasPortfolio": false, "registerNotValidatedReason": "Accepted terms not valid, E-mail not verified, address not valid, document not valid, personalProfile not valid, investmentProfile not valid, bankAccounts not found", "shouldUpdateAccount": false, "hasVisitedWelcomeScreen": false, "crmCommunicationType": "auto", "acceptedTerms": false, "acceptedTermsCount": 0, "goWaitList": false, "canCreateInvoice": true, "notificationsConfig": [{"id": "user_config_transactions", "email": true, "push": true }, {"id": "user_config_remember_monthly_investments", "email": true, "push": true }, {"id": "user_config_daily_report", "push": false }, {"id": "user_config_news", "push": true }, {"id": "user_config_warren_communication", "email": true }, {"id": "user_config_account_update", "email": true, "push": true } ], "depositAcceptedCount": 0, "waitlistUnlockedVia": "suitability", "waitlistUnlockedAt": "2020-09-03T13:46:00.755Z", "referralId": "VQUdkh", "referredBy": "", "clientTheme": "pink", "features": ["NEW_SUITABILITY", "portfolio_background", "portfolioTransfer", "user_positions", "builder_edit_instrument_box", "managedPortfolio", "summary_chart", "deposit_cart", "cashback", "portfolio_shared", "summary_screen", "builder_edit_instrument", "portfolio_performance_history", "CONVERSATION_CARDS_LIQUIDATION", "REGISTER_AUTO_APPROVED", "APP_THEME", "TRANSFER_BANKING_TO_BROKER", "CASHBACK", "SUMMARY_SCREEN", "SUMMARY_CHART", "PORTFOLIO_PERFORMANCE_HISTORY", "BANKING_PAYMENTS", "DASH_V2", "GLOBAL_SUMMARY", "NEW_REGISTER_VUE", "TRANSFER_BROKER_TO_BANKING", "TRANSFER_CHECKING_TO_BANKING", "CACHE_SERVICE", "CREATE_PAYMENT_REWARD", "INVITES_CONTENT_CAMPAIGN", "REMINDER_MONTHLY_INVESTMENT", "DIRECT_DEBIT", "PRIVATE_PENSION", "PORTFOLIO_EDIT_ALLOCATIONS", "SUITABILITY_VUE", "PARTNER_CREATE_INVESTOR_ACCOUNT", "BANKING_BUILD_PAYMENTS_RECEIPT", "SUITABILITY_SCORE_RISK", "UPDATE_ACCOUNT_TO_REGULATION", "SUITABILITY_EMAIL_SUGGESTER", "CS_CHAT", "WALLET_BUILDER", "WARREN_VERSION_3", "OVERVIEW_3_0", "B2B_B2C_SAME_SUITA", "COST_AREA_V2", "ADD_NEW_USER_TO_BANKING", "SUITABILITY_PASSWORD_QUESTION", "WEALTH_3_0", "CASH_3_0", "TRADE_3_0", "WITHDRAWAL_BY_PRODUCT", "SIGNIN-WITH-APPLE", "COSTS_AREA_V2_MONTH_DETAILS", "COSTS_AREA_V2_FAQ", "B2B_B2C_UNIFIED_LOGIN_VIEW", "CREATE_CTVM_ACCOUNT", "TRANSFER_TRADE_TO_BANKING", "WITHDRAW_SPB", "ACTIVE_APP_VERSION", "CASH_SUMMARY_TAXES_TO_NEGATIVE", "WITHDRAWAL_TO_WARREN_ACCOUNT", "PARTNER_DASH_V2", "WALLET_REINVESTMENT_BEHAVIOR", "HUBSPOT_CASH_ACCOUNTS_RETRY", "B2C_UNIFIED_LOGIN", "B2B_3_0_BRAND_ROLLOUT", "DARK_MODE", "TRADING_AVAILABLE", "TRADE_ORDER_ON_DASH_STATUS", "SPB_BROKER_CONCILIATION", "BANKING_PAYMENTS_PAGUE_VELOZ", "IMPERSONATE_LEADS", "WALLET_TRANSFER_FROM_CORE"], "promotionalCode": null, "hasExternalInfo": false, "availableExternalQuiz": false, "verifiedByExternalInfo": false, "address": {}, "document": {}, "personalProfile": {}, "investmentProfile": {"_id": "5f50f399fb72f10017cc2d1e", "customer": "5f50f398fb72f10017cc2d19", "monthlyIncome": 7000, "isQualifiedInvestor": false, "computedProfileType": "practical", "isValid": false, "isVerified": false, "status": "deposit not set", "computedRiskTolerance": 19.5, "riskToleranceProfile": "Conservador"}, "suitability": {"_id": "5f50f399fb72f10017cc2d1b", "customer": "5f50f398fb72f10017cc2d19", "computedProfileType": "practical", "question_name": "jao", "question_income": 7000, "question_interest": "B", "question_learning": "unknown", "question_market": "unknown", "question_preference": "A", "question_read": "unknown", "question_spend": "unknown", "question_advice": "unknown", "question_isretired": "unknown", "question_lottery": "unknown", "question_investment_experience": "unknown", "question_invest": "A", "question_product": ["prefixed", "multimarket", "stocks"], "question_investment_time": "1-3", "question_after_invest": "unknown", "question_autonomy": "unknown", "question_context": "unknown", "question_motivation": "unknown", "question_planning": "B", "question_economy": "B", "computedRiskTolerance": 19.5 }, "bankAccounts": [], "suitabilityFinishedCount": 1, "configs": {} }, "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcklkIjoiNWY1MGYzOThmYjcyZjEwMDE3Y2MyZDE5IiwiYWNjZXNzVG9rZW5JZCI6IjVmNTBmMzk5ZmI3MmYxMDAxN2NjMmQxYyIsImFjY2Vzc1Rva2VuSGFzaCI6IjI5YzkwYjc0NGYyNmM5YjVlYzM1YjIwMWFlYTUwM2M4YWQ4OWVhY2IyODU2MDI2OGMxNmU3YmIzMDZmNWExZWMiLCJpYXQiOjE1OTkxNDA3NjIsImV4cCI6MTU5OTc0NTU2Mn0.mawNWqMSUp2_7d2nxVuW64HJaPsXlHR_ClC9H3HRhCI", "refreshToken": "ef18638e84ea517c29a0522650ae54df55fbdf116fdfd67f90b8809015029ec1", "sameProfileCounter": 11400 }
      }

      const transition = Math.random() * 1800
      setTimeout(() => {
        Store.dispatch(actions.IS_LOADING, false)
        resolve({ data: response })
      }, transition)
    })
  },

  async get() {
    return {
      response: {"title": "Prático", "headline": "“Quero ter certeza que meu dinheiro está no melhor lugar”", "coverPictureURL": {"raw": null, "full": "https://assets.warrenbrasil.com/images/suitability/01_pratico.jpg", "regular": "https://assets.warrenbrasil.com/images/suitability/01_pratico_mobile.jpg", "small": null, "thumb": null }, "sections": [{"type": "content", "title": "Investir bem sem esquentar a cabeça", "content": [{"type": "text", "content": "Você sabe da importância de investir, mas não quer se preocupar com isso. Quer transparência e confiança para ver seu dinheiro render com a certeza de que ele está em boas mãos."} ] }, {"type": "media", "title": "Na plataforma você vai", "content": [{"type": "image", "alt": "Texto alternativo da imagem", "url": {"raw": null, "full": "https://assets.warrenbrasil.com/images/suitability/02_pratico.jpg", "regular": "https://assets.warrenbrasil.com/images/suitability/02_pratico_mobile.jpg", "small": null, "thumb": null } }, {"type": "list", "content": ["Conferir todos os detalhes sobre os seus rendimentos na tela do celular e no computador a qualquer momento.", "Receber relatórios transparentes e detalhados sobre a evolução do seu patrimônio.", "Ter atendimento com pessoas reais, preparadas para ajudar você"] } ] } ] }
    }
  },
})