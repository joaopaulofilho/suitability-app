import Vue from 'vue'
import axios from 'axios'
import Store, { actions } from 'src/store'
import FakeAxios from './axios.fake'

const http = axios.create({
  baseURL: 'https://api.dev.oiwarren.com/api/v2',
})

http.interceptors.request.use(
  config => (
    Store.dispatch(actions.IS_LOADING, true),
    config
  ),
  console.error
)

http.interceptors.response.use(
  config => (
    Store.dispatch(actions.IS_LOADING, false),
    config
  ),
  console.error
)

Vue.prototype.$axios = http

// usado pelo tempo em que API dev ficou com erros, na manhã de sexta 4/set/2020
// Vue.prototype.$axios = FakeAxios()