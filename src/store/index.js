import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

const Store = new Vuex.Store({
  modules: {
    // example
  },

  state: {
    isLoading: true,
    profile: {},
  },

  getters: {
    loading: ({ isLoading }) => isLoading,
    profile: ({ profile }) => profile,
  },

  mutations: {
    SET_IS_LOADING(state, newVal) {
      state.isLoading = newVal
    },

    SAVE_PROFILE(state, profile) {
      state.profile = profile
    },
  },

  actions: {
    setIsLoading({ commit }, newVal = true) {
      commit('SET_IS_LOADING', newVal)
    },

    saveProfile({ commit }, profile) {
      if (!profile) {
        console.error('Profile must have suitability user data')
      }

      commit('SAVE_PROFILE', profile)
    }
  },

  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV
})

const actions = {
  IS_LOADING: 'setIsLoading',
}

export default Store

export { actions }
